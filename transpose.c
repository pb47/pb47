#include<stdio.h>
int main()
{
 int arr[20][20];
 int i=0,j=0;
 int m,n;
 printf("Enter the size of the matrix: \n");
 printf("ROWS: ");
 scanf("%d",&m);
 printf("COLUMNS: ");
 scanf("%d",&n);
 printf("Enter the elements of the matrix: \n");

 for(i=0;i<m;i++)
 {
  for(j=0;j<n;j++)
  {
   scanf("%d",&arr[i][j]);
  }
 } 

 printf("The  given matrix : \n");

  for(i=0;i<m;i++)
 {
  for(j=0;j<n;j++)
  {
   printf("%d \t",arr[i][j]);
  }
  printf("\n");
 } 

 printf("The transpose: \n");

 for(i=0;i<m;i++)
 {
  for(j=0;j<n;j++)
  {
   printf("%d \t",arr[j][i]);
  }
  printf("\n");
 } 

 return 0;
}