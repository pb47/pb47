include<stdio.h>
int main()
{
 int i,j, max=0;
 int arr[5][3];
 printf("Enter the marks ");
 for(i=0;i<5;i++)
 {
  printf("For student No.%d: \n",i+1);
  for(j=0;j<3;j++)
  {
   printf("For subject No.%d: ",j+1);
   scanf("%d",&arr[i][j]);
  }
 }
 printf("The student and marks matrix: \n");
 for(i=0;i<5;i++)
 {
   printf("Student %d: ",i+1);
  for(j=0;j<3;j++)
  {
    printf("Subject %d: ",j+1);
    printf("%d   ",arr[i][j]);
  }
  printf("\n");
 }
 
 for(j=0;j<3;j++)
 {
  max=arr[0][j];
  for(i=0;i<5;i++)
  {
   if(max<arr[i][j])
    max=arr[i][j];
  }
 printf("The highest marks for subject No.%d is %d. \n",j+1,max); 
 }
 return 0;
}

