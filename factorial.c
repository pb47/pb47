#include<stdio.h>
int factorial (int num);
int main()
 {
   int num,fac;
   printf("Enter the number");
   scanf("%d",&num);
   fac = factorial(num);
   printf("Factorial of %d is %d",num,fac);
   return 0;
 }

int factorial(int num)
 {
  int fac=1,i;
  
  if(num==0)
    fac=1;
  else
    {
     for(i=1;i<=num;i++)
       fac=fac*i;
    }
 return(fac);
 }
 