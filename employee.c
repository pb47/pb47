#include<stdio.h>

struct employee
 {
  char name[20];
  float salary;
  int empID;
  char doj[20];
  char desg[20];
 };

int main()
 {
   struct employee emp;
   
   printf("Enter the name of the employee : ");
   fgets(emp.name,19,stdin);
   printf("\nEnter the salary of the employee : ");
   scanf("%f",&emp.salary);
   printf("\nEnter the employee ID : ");
   scanf("%d",&emp.empID);
   printf("\nEnter the date of joining in DD/MM/YYYY format : ");
   scanf("%s",emp.doj);
   printf("\nEnter the designation : ");
   scanf("%s",emp.desg);
 
   printf("The name of the employee : %s",emp.name);
   printf("\nThe salary of the employee : %f",emp.salary);
   printf("\nThe ID of the employee : %d",emp.empID);
   printf("\nThe date of joining of the employee : %s",emp.doj);
   printf("\nThe designation of the employee : %s",emp.desg);
 
   return 0;
  }