#include<stdio.h>
void swap(int *a, int *b)
{
  int temp;
  temp = *a;
  *a = *b;
  *b = temp;
}
int main()
{
  int n1, n2;
  printf("Enter the first number : ");
  scanf("%d",&n1);
  printf("\nEnter the second number : ");
  scanf("%d",&n2);

  printf("\nThe value of n1 and n2 before swapping : %d %d",n1,n2);
  swap(&n1,&n2);

  printf("\nThe value of n1 and n2 after swapping : %d %d",n1,n2);
 
  return 0;
}